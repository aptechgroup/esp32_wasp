class UserCallback : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pUserCharacteristic)
  {
    std::string value = pUserCharacteristic->getValue();
    if (value.length() > 0)
    {
      String nickname = "";

      for (int i = 0; i < value.length(); i++)
      {
        Serial.print(value[i]);
        nickname += value[i];
      };
      dataStorage.begin("wasp", false); // open WASP dataStorage in R/W mode
      dataStorage.putString("nickname", nickname);
      dataStorage.end();

      BLEAdvertisementData advertisementData;
      char buffer[11];                              // buffer for BLE name, max 10 characters
      nickname.toCharArray(buffer, sizeof(buffer)); // convert nickname to a character array
      pAdvertising->stop();
      advertisementData.setName(buffer);                            // use nickname as BLE name
      advertisementData.setCompleteServices(BLEUUID(SERVICE_UUID)); // advertise the service uuid so app can find it
      pAdvertising->setAdvertisementData(advertisementData);
      pAdvertising->start();
    }
  }

  void onRead(BLECharacteristic *pUserCharacteristic)
  {

    int lastFillTime = round((currentMillis - lastFill) / 3600000);  // calculate last fill in hours


    // FOR RELEASE VERSION DO NOT USE FULL KEY NAMES

    StaticJsonBuffer<300> jsonBuffer; // buffer for data sent to app
    JsonObject &root = jsonBuffer.createObject();
    root["version"] = VERSION;
    root["condition"] = condition;
    root["raw"] = rawLevel;
    root["current"] = currentLevel;
    root["average"] = averageLevel;
    root["fills"] = fills;
    root["total_fills"] = fills + data_fills;
    root["boots"] = data_boots;
    root["waves"] = waveCounter;
    root["total_waves"] = totalWaveCounter;
    root["fill_counter"] = fill_counter;
    root["fill_limit"] = FILL_LIMIT;
    root["last_fill"] = lastFillTime;
    JsonArray &averageArray = root.createNestedArray("average_array");
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
      averageArray.add(levelArray[i]);
    }
    root.prettyPrintTo(Serial);
    char buffer[300];

    root.printTo(buffer, sizeof(buffer));
    pUserCharacteristic->setValue(buffer); // save json data to characteristic
  }
};

class DebugSprayCallback : public BLECharacteristicCallbacks    // callback for testing spray
{
  void onWrite(BLECharacteristic *pDebugSprayCharacteristic)
  {
    std::string value = pDebugSprayCharacteristic->getValue();

    if (value.length() > 0 && value.length() <= 2)
    {
      // test the primary solenoid spray for X amount of seconds
      digitalWrite(PRIMARY_SOLENOID, HIGH); // Turn on primary solenoid
      delay(atoi(value.c_str()) * 1000);    // delay for leaving ON in seconds
      digitalWrite(PRIMARY_SOLENOID, LOW);  // Turn OFF primary solenoid
    }
  }
};

class DebugResetCallback : public BLECharacteristicCallbacks  // callback to reset the dissolver
{
  void onWrite(BLECharacteristic *pDebugResetCharacteristic)
  {
    std::string value = pDebugResetCharacteristic->getValue();

    if (value.length() > 0)
    {
      Serial.println("Resetting the ESP32");
      ESP.restart();
    }
  }
};

class ServerCallback : public BLEServerCallbacks    // callback for when device is connected
{
  void onConnect(BLEServer *pServer)
  {
    deviceConnected = true;
    digitalWrite(PRIMARY_SOLENOID, LOW); // turn off the primary solenoid while connected
  };

  void onDisconnect(BLEServer *pServer)
  {
    deviceConnected = false;
  }
};