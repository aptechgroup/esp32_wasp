void operate_solenoids()
{

  int previousCycle = digitalRead(PRIMARY_SOLENOID);
  Serial.println("**********");
  Serial.println(digitalRead(PRIMARY_SOLENOID));

  switch (condition)
  {
  case 4:                                // ERROR - LEAK DETECTED
  case 2:                                // ERROR - OVERFLOW
    digitalWrite(PRIMARY_SOLENOID, LOW); // make sure primary solenoid is OFF
    bleOverflow = true;
    bleAlarm = true;
    break;

  case 3:                                // ERROR - ULTRASONIC ERROR
  case 5:                                // ERROR - NO WATER FLOW
  case 6:                                // ERROR - SOLENOID NOT CONNECTED
    digitalWrite(PRIMARY_SOLENOID, LOW); // make sure primary solenoid is OFF
    bleOverflow = false;
    bleAlarm = true;
    break;

  case 0:                                // IDLE
    digitalWrite(PRIMARY_SOLENOID, LOW); // make sure primary solenoid is OFF
    bleOverflow = false;
    bleAlarm = false;
    break;

  case 1:                                 // FILLING
    digitalWrite(PRIMARY_SOLENOID, HIGH); // turn ON primary solenoid
    bleOverflow = false;
    bleAlarm = false;
    break;
  }

  if (condition == 5 || condition == 6) // solenoid not connected or no water flowing
  {
    if ((currentMillis - PERIODIC_previousMillis) >= PERIODIC_INTERVAL) // wait for defined period of time
    {
      condition = 1;        // go back to trying to fill
      periodicCheck = true; // let it be known that we are checking
      
    }
    else
    {
      return;
    }
  }

  if (!periodicCheck) // only run end fill cycle code when we aren't checking for condition 5 and 6 periodically
  {
    if (digitalRead(PRIMARY_SOLENOID) != previousCycle && digitalRead(PRIMARY_SOLENOID) == LOW) // end of fill cycle
    {
      Serial.println("End fill cycle");
      fills++;
      dataStorage.begin("wasp", false);                            // open WASP dataStorage in R/W mode
      dataStorage.putUInt("data_fills", (data_fills + fills));     // save total number of fills over lifetime of unit
      dataStorage.putUInt("fill_counter", (fills + fill_counter)); // save fill_counter fills to predict usage
      dataStorage.end();

      setLeakLevel = true;                 // start counter for leak check
      LEAK_previousMillis = currentMillis; // current time for waiting for things to settle
      lastFill = currentMillis;            // save the last time a fill cycle completed

      setShuntBaseline = true;
      SHUNT_previousMillis = currentMillis;   // current time to wait for shunt test
    }
  }

  if (digitalRead(PRIMARY_SOLENOID) != previousCycle && digitalRead(PRIMARY_SOLENOID) == HIGH) // beginning of fill cycle
  {
    Serial.println("Begin fill cycle");
    FLOW_previousMillis = currentMillis;  // save current time to check flow
    flowLevel = averageLevel;   // set the current level to check flow against
    checkFlow = true;     // turn on bool for checking flow
    Serial.println("****** START WATER CHECK *******");
    SHUNT_previousMillis = currentMillis;   // save current time to check shunt level
    checkShuntReading = true;       // turn on bool for checking shunt
  }

  if (digitalRead(PRIMARY_SOLENOID) == HIGH && !isSolenoidConnected())
  {
    condition = 6;
    PERIODIC_previousMillis = currentMillis; // save current time to start timer
    digitalWrite(PRIMARY_SOLENOID, LOW);     // turn off the solenoid
    return;                                  // stop running the rest of the code since the solenoid isn't working
  }

  if (setShuntBaseline && digitalRead(PRIMARY_SOLENOID) == LOW && (currentMillis - SHUNT_previousMillis) >= SHUNT_INTERVAL)   // check shunt base level after fill cycle
  {
    setShuntBaseline = false;     // turn off bool for checking shunt
    shuntBaseline = analogRead(SHUNT_PIN);    // set the shunt baseline for when the solenoid is off
  }

  if (checkFlow && (currentMillis - FLOW_previousMillis) >= FLOW_INTERVAL)
  {
    Serial.println("++++++ TIME TO CHECK THE WATER +++++++");
    checkFlow = false;
    if (!isWaterFlowing())
    {
      condition = 5;
      PERIODIC_previousMillis = currentMillis; // save current time to start timer
      digitalWrite(PRIMARY_SOLENOID, LOW);     // turn off the solenoid
      Serial.println("**** NO WATER FLOW ****");
    }
    else
    {
      periodicCheck = false; // turn off the periodic check flag because everything is good
    }
  }
  if ((fills + fill_counter) >= FILL_LIMIT)
  {
    bleProductChange = true;
  }
  else
  {
    bleProductChange = false;
  }
}

bool isSolenoidConnected() // determine if the solenoid is connected and functioning
{
  // return true; // always return true for now

  if (digitalRead(PRIMARY_SOLENOID) == LOW)
  {
    return true;
  }
  else if (checkShuntReading && (currentMillis - SHUNT_previousMillis) >= SHUNT_INTERVAL){
    checkShuntReading = false;  // turn off bool for checking shunt
    int shuntReading = analogRead(SHUNT_PIN);
    if ((shuntReading - shuntBaseline) >= SHUNT_LIMIT)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return true;
  }
}

bool isWaterFlowing()
{
  if (averageLevel <= (flowLevel - FLOW_LEVEL_CHANGE))
  {
    Serial.println("WATER GOOD");
    return true;
  }
  else
  {
    Serial.println("WATER BAD");
    return false;
  }
}