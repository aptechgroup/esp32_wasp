void level_reading()
{
  digitalWrite(TRIG_PIN, LOW); // clear the TRIG_PIN
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  rawLevel = (int)pulseIn(ECHO_PIN, HIGH, 60000);

  Serial.print(arrayIndex);
  Serial.print(": ");
  Serial.print(rawLevel);
  Serial.print(" --> ");
  Serial.println(averageLevel);

  if (rawLevel > (BOTTOM_LEVEL + DISTANCE_DRIFT)) // distance read is greater than the bottom + drift
  {
    Serial.println("Below Bottom");
    errorReadings++; // increase ultrasonic error counter;
    return;          // do no continue with saving the reading into data
  }
  else if (rawLevel < (TOP_LEVEL - DISTANCE_DRIFT)) // distance read is less than the top - drift
  {
    Serial.println("Above Top");
    errorReadings++; // increase ultrasonic error counter;
    return;          // do no continue with saving the reading into data
  }
  else
  {
    Serial.println("Good reading");
    currentLevel = rawLevel;
    errorReadings = 0; // reset error counter on a good reading;
  }

  if (!initialBoot) // don't check for waves on the initial boot
  {
    // check to see if level distance is too far away from the average
    if (digitalRead(PRIMARY_SOLENOID) == LOW && abs(currentLevel - averageLevel) >= WAVE_DISTANCE) //only check for waves if we aren't filling
    {
      Serial.println("wave in affect");
      waveCounter++;      // increase the wave counter
      totalWaveCounter++; // increase the total wave counter
      if (waveCounter >= MAX_WAVES)
      {
        memset(levelArray, 0, sizeof(levelArray));
        arrayIndex = 0;
        initialBoot = true;
        waveCounter = 0;
        digitalWrite(PRIMARY_SOLENOID, LOW);
        return;
      }
      return; // do not continue if it is a wave, it will not get included in the average
    }
    else
    {
      waveCounter = 0; // reset wave counter on a reading that is within range
    }
  }

  levelArray[arrayIndex] = currentLevel; // save current level into array at defined arrayIndex
  arrayIndex++;                          // move to next array position
  if (arrayIndex == ARRAY_SIZE)
  {
    arrayIndex = 0; // move to beginning if at the end
    initialBoot = false;
  }
  long readingSum = 0L;   // sum of good readings used for average
  int readingCount = 0;  // count of readings in level array
  for (int i = 0; i < ARRAY_SIZE; i++)
  {
    if (levelArray[i] != 0 && !isnan(levelArray[i]))
    {
      readingSum += levelArray[i];    // add reading to reading sum
      readingCount ++;       // increase reading count
    }
  }
  averageLevel = (int)readingSum / readingCount; // save average reading (sum / count)
}
