void led_control()
{
  if (deviceConnected)
  {
    if (currentMillis - LED_previousMillis >= LED_INTERVAL)
    {
      // save the last time you blinked the LED
      LED_previousMillis = currentMillis;
      if (digitalRead(LED_STATUS) == HIGH)
      {
        digitalWrite(LED_STATUS, LOW);
        digitalWrite(LED_ERROR, HIGH);
      }
      else
      {
        digitalWrite(LED_STATUS, HIGH);
        digitalWrite(LED_ERROR, LOW);
      }
    }
  }
  else
  {

    switch (condition)
    {
    case 0:
      digitalWrite(LED_STATUS, HIGH);
      digitalWrite(LED_ERROR, LOW);
      break;

    case 1:
      if (currentMillis - LED_previousMillis >= LED_INTERVAL)
      {
        LED_previousMillis = currentMillis;
        digitalWrite(LED_STATUS, !digitalRead(LED_STATUS));
      }
      digitalWrite(LED_ERROR, LOW);
      break;

    case 2:
      if (currentMillis - LED_previousMillis >= LED_INTERVAL)
      {
        LED_previousMillis = currentMillis;
        if (digitalRead(LED_STATUS) == HIGH)
        {
          digitalWrite(LED_STATUS, LOW);
          digitalWrite(LED_ERROR, LOW);
        }
        else
        {
          digitalWrite(LED_STATUS, HIGH);
          digitalWrite(LED_ERROR, HIGH);
        }
      }
      break;

    default:
      if (currentMillis - LED_previousMillis >= LED_INTERVAL)
      {
        LED_previousMillis = currentMillis;
        digitalWrite(LED_ERROR, !digitalRead(LED_ERROR));
      }
      digitalWrite(LED_STATUS, LOW);
      break;
    }
  }
}