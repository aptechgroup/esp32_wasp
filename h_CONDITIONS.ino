void set_condition()
{
  if (condition == 5 || condition == 6)   // if no solenoid or flow, don't change condition.
  {
    return;
  }
  if (errorReadings >= MAX_ERRORS) // check for too many over/under consecutive readings
  {
    condition = 3;
    return; // don't run rest of the solenoid operation
  }

  if (!setLeakLevel && !initialBoot && condition != 1 && (leakLevel - averageLevel) > LEAK_DISTANCE)
  { // check that its not waiting for level, not initial boot, not filling, and beyond the leak distance
    condition = 4;
    return; // don't continue once leak is detected
  }

  if (averageLevel < HIGH_LEVEL)
  { // check for overflow condition against average level
    condition = 2;
  }
  else if (averageLevel > FILL_LEVEL)
  {                // check if it needs to fill against the average level
    condition = 1; // set condition to filling
  }
  else if (averageLevel < FULL_LEVEL)
  {                // check if it is full against the average level
    condition = 0; // set condition to idle
  }
}