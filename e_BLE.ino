
/* ADVERTISED DATA LAYOUT

12345
1 & 2 = manufacturer code
3 = status
4 = overflow solenoid
5 = change bottle

*/

void setup_ble()
{

  BLEDevice::init("aptg");                                     // set up generic name for BLE
  pServer = BLEDevice::createServer();                         // create the server
  pServer->setCallbacks(new ServerCallback());                 // setup callback for device connection detection
  BLEService *pService = pServer->createService(SERVICE_UUID); // create BLE service

  pUserCharacteristic = pService->createCharacteristic(
      USER_CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_WRITE | BLECharacteristic::PROPERTY_READ); // create BLE characteristic with READ and WRITE

  pUserCharacteristic->setCallbacks(new UserCallback()); // callback for charatacteristic to get data and write data

  BLECharacteristic *pDebugSprayCharacteristic = pService->createCharacteristic(
      DEBUG_SPRAY_CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_WRITE);
  pDebugSprayCharacteristic->setCallbacks(new DebugSprayCallback());

  BLECharacteristic *pDebugResetCharacteristic = pService->createCharacteristic(
      DEBUG_RESET_CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_WRITE);
  pDebugResetCharacteristic->setCallbacks(new DebugResetCallback());

  pService->start();

  pAdvertising = pServer->getAdvertising();
  BLEAdvertisementData advertisementData;

  char buffer[11];                                              // buffer for BLE name, max 10 characters
  data_nickname.toCharArray(buffer, sizeof(buffer));            // convert nickname to a character array
  advertisementData.setName(buffer);                            // use nickname as BLE name
  advertisementData.setCompleteServices(BLEUUID(SERVICE_UUID)); // advertise the service uuid so app can find it
  pAdvertising->setAdvertisementData(advertisementData);
  pAdvertising->start(); // start advertising
}

void update_ble()
{
  // int bleStatus;

  // if (condition <= 1) // filling or idle is status condition 0  **good**
  // {
  //   bleStatus = 0;
  // }
  // else if (condition == 2) // overflow is status condition 2 **warning**
  // {
  //   bleStatus = 1;
  // }
  // else // everything else is status condition 3 **alert**
  // {
  //   bleStatus = 2;
  // }

  String advertisedDataNew; // variable to hold manufacturer data
  int m_code = man_code;    // required manufacturer code for bluetooth
  int levelPercent = (BOTTOM_LEVEL - averageLevel)/(BOTTOM_LEVEL - TOP_LEVEL);  // Percent full solution level
  char b2 = (char)(m_code >> 8);
  m_code <<= 8;
  char b1 = (char)(m_code >> 8);
  advertisedDataNew.concat(b1);
  advertisedDataNew.concat(b2);
  // advertisedDataNew.concat(bleStatus);   // add the status condition to the payload  
  advertisedDataNew.concat(condition);   // add the status condition to the payload
  advertisedDataNew.concat(levelPercent); // add the solution level to the paylaod
  advertisedDataNew.concat(bleOverflow);  // add boolean for overflow solenoid
  advertisedDataNew.concat(bleAlarm);   // add boolean for alarm relay
  advertisedDataNew.concat(bleProductChange); // add boolean for if product needs to be changed

  if (advertisedDataOld != advertisedDataNew) // only update if things have changed
  {
    advertisedDataOld = advertisedDataNew;    // save new adverstised data
    BLEAdvertisementData scan_response;
    scan_response.setManufacturerData(advertisedDataNew.c_str()); // set the adverstisment data for the scan response
    pAdvertising->stop();                                         // stop adverstising
    pAdvertising->setScanResponseData(scan_response);             // change the scan response so it has the current status condition
    pAdvertising->start();                                        // start advertising again
  }
}