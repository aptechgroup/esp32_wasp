
  This program is developed by David Naylor with rights held by APTech Group, Inc.

  versions:
  1 - initial

--------------
LEVEL READINGS
--------------

LEVEL READING --> The dissolver uses a rolling average for level control.  The ARRAY_SIZE defines how many points are used for this rolling average.  The individual level readings are held in an array.

WAVES -->  A wave is when the distance reading is greater than WAVE_DISTANCE from the average reading.  A wave counter counts the consecutive waves, any non-wave reading will reset the counter.  Once the wave counter reaches MAX_WAVES, the level readings array is cleared out and repopulated.  This is to not react to 1 or 2 "errant" readings.


----------
CONDITIONS
----------

0 = idle
1 = filling
2 = overflow
3 = ultrasonic error
4 = leak detected
5 = water not flowing
6 = solenoid not connected/faulty


0 - IDLE -->  When the average level is between the FILL_LEVEL and the HIGH_LEVEL and is not filling.
    Solenoid = Off
    Alarm = False
    Overflow = False


1 - FILLING -->  Triggered when the average level reaches the FILL_LEVEL.  The solenoid will be energized to allow water the flow.  Once the level reaches the FULL_LEVEL, the solenoid will be de-energized and condition will go to 0 - IDLE.
    Solenoid = On
    Alarm = False
    Overflow = False


2 - OVERFLOW -->  Triggered when the average level reaches the HIGH_LEVEL.  This will send out an alert to the overflow solenoid via BLE.
    Solenoid = Off
    Alarm = True
    Overflow = True
   

3 - ULTRASONIC ERROR -->  Distance reading is either longer than the BOTTOM_LEVEL + DRIFT_DISTANCE or shorter than the TOP_LEVEL + DRIFT_DISTANCE.  The BOTTOM_LEVEL is the bottom of an empty dissolver and the TOP_LEVEL is the top of the overflow tube.  DRIFT_DISTANCE is just a fudge factor.  These errors have to be consecutive.  Every good reading resets the error counter.  Once consecutive error reach MAX_ERRORS this condition is triggered.
    Solenoid = Off
    Alarm = True
    Overflow = False
   

4 - LEAK DETECTED  -->  Once a fill cycle is completed, a time is recorded.  After LEAK_INTERVAL is reached, the solution level is recorded.  If the solution level increases more than LEAK_DISTANCE without the solenoid ever being energized, this condition is triggered.
    Solenoid = Off
    Alarm = True
    Overflow = True
   

5 - WATER NOT FLOWING -->  At the beginning of a fill cycle, the solution level is recorded.  After FLOW_INTERVAL is reached, the solution level is checked again.  If the increase in solution level is not at least FLOW_DISTANCE then this condition will be triggered.  Once in this condition, filling will be retried after PERIODIC_INTERVAL.
    Solenoid = Off
    Alarm = True
    Overflow = False
   

6 - SOLENOID NOT CONNECTED/FAULTY -->  During startup a shunt baseline reading is taken when the solenoid is off.  After SHUNT_INTERVAL from when the solenoid is energized, a reading is taken from the shunt resistor.  If that reading is not SHUNT_LIMIT higher than the baseline, it is assumed that either the solenoid is not connected or it is faulty.  If that is the case, this error will be triggered.  The baseline is reset after SHUNT_INTERVAL from the end of a fill cycle.  Once in this condition, filling will be retried after PERIODIC_INTERVAL.
    Solenoid = Off
    Alarm = True
    Overflow = False
   


--------------------------
BLUETOOTH LOW ENERGY (BLE)
--------------------------

ADVERTISING DATA -->  The unit will broadcast a byte array along with a dissolver nickname.  This data can be read by a client without actually needed to connect to this device.  The advertised data gets updated as things change with the unit.  The byte array is as follow:  [a, b, c, d, e, f, g]

    a -->  part 1 of 2 of manufacturer code
    b -->  part 2 of 2 of manufacturer code
    c -->  condition (integer)
    d -->  solution level (integer)
    e -->  overflow trigger (boolean)
    f -->  alarm trigger (boolean)
    g -->  product change prediction trigger (boolean)

MOBILE APP -->  The user will be able to run a scan for all nearby dissolvers.  A list is generated displaying the dissolver nickname and a status icon (good, warning, error) that is dependent on the dissolver condition.  Once the user selects a dissolver to connect to they will be able to:

    1.  Change the dissolver nickname.
    2.  Reset the dissolver.
    3.  Turn on the spray for a selectable amount of seconds (i.e. 1, 2, 5, 10...).
    4.  Generate an email to send to APTech that has all the data.
    5.  View data.

OVERFLOW SOLENOID -->  The overflow solenoid is a normally open (NO) solenoid that is integrated into an incoming water manifold.  The electronics of this manifold scan for nearby dissolvers every 30 seconds.  It looks at the 4th byte of the advertised data to determine if a dissolver is requesting that the overflow solenoid be activated.  If one is advertising a true value, the solenoid will be energized and water flow will be turned off to the manifold.

ALARM RELAYS -->  The alarm relay module will scan for nearby dissolvers every 30 seconds.  It'll look at the 5th and 6th byte of the advertised data.  There will be 2 relays on the module that are hooked up to dry contacts in the controller.  The first relay is for alarms and the second is for product remaining prediction.  If the module sees a true value in the advertised data, it'll close the associated relay.  This will complete that circuit for the dry contact and the controller will react.
