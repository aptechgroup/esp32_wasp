

void setup()
{
  Serial.begin(115200);
  Serial.println("\nBooting Project WASP ESP32\n");

  // set initial state of pins
  digitalWrite(PRIMARY_SOLENOID, LOW);
  digitalWrite(LED_STATUS, LOW);
  digitalWrite(LED_ERROR, LOW);
  digitalWrite(TRIG_PIN, LOW);

  // define each pin type
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT_PULLUP);
  pinMode(PRIMARY_SOLENOID, OUTPUT);
  pinMode(LED_STATUS, OUTPUT);
  pinMode(LED_ERROR, OUTPUT);
  
  // check functionality
  digitalWrite(PRIMARY_SOLENOID, HIGH);
  delay(20);
  digitalWrite(PRIMARY_SOLENOID, LOW);

  // set shunt baseline
  delay(100);   // wait for power to disapate from test
  shuntBaseLine = analogRead(SHUNT_PIN);    // record shunt level for baseline

  setup_data();           // load data from nonvolatile storage
  setup_ble();            // set up the BLE server
}

void setup_data()
{
  dataStorage.begin("wasp", false);                  // open WASP dataStorage in R/W mode
  data_boots = dataStorage.getUInt("data_boots", 0); // get number of starts
  data_fills = dataStorage.getUInt("data_fills", 0);
  FILL_LIMIT = dataStorage.getUInt("fill_limit", 500);   // fill limit defaults to 500 if nothing is set by user
  fill_counter = dataStorage.getUInt("fill_counter", 0); // get fill_counter value
  data_nickname = dataStorage.getString("nickname", "enduroTEQ");
  data_boots++; // increase starts by 1
  dataStorage.putUInt("data_boots", data_boots);
  dataStorage.end();
}









