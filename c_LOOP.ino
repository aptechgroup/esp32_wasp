
void loop()
{
  currentMillis = millis();
  if (currentMillis - LEVEL_previousMillis >= LEVEL_INTERVAL)
  {
    LEVEL_previousMillis = currentMillis; // save the last time this loop ran
    level_reading();                      // get and analyze level reading
    set_condition();                      // set the condtion
    if (!deviceConnected && !initialBoot)                 // only operate solenoids if device is not connected
    {
      operate_solenoids(); // operate the solenoids
    }
    update_ble(); // update Scan Response Data for BLE
  }
  if ((currentMillis - LEAK_previousMillis) >= LEAK_INTERVAL && setLeakLevel) // set leak detection parameters
  {
    setLeakLevel = false;
    leakLevel = averageLevel; // set the starting leak level
  }
  led_control();
}