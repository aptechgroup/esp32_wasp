
#include <Preferences.h> // used for data storage
#include <BLEDevice.h>   // used for BLE connection
#include <BLEUtils.h>    // used for BLE connection
#include <BLEServer.h>   // used for BLE connection
#include <ArduinoJson.h> // used for generating data to send via BLE

const int VERSION = 1.0.1;

// setup BLE variables
BLEServer *pServer = NULL;
BLECharacteristic *pUserCharacteristic = NULL;
BLEAdvertising *pAdvertising;
bool deviceConnected = false;
String advertisedDataOld = "";
bool bleOverflow = false;   // trigger overflow solenoid
bool bleProductChange = false;
bool bleAlarm = false;

int man_code = 0x02E5; // manufacturer code (0x02E5 for Espressif)

#define SERVICE_UUID "4e9dea20-3da7-11e8-b467-0ed5f89f718b"
#define USER_CHARACTERISTIC_UUID "4e9dea21-3da7-11e8-b467-0ed5f89f718b"
#define DEBUG_SPRAY_CHARACTERISTIC_UUID "4e9dea22-3da7-11e8-b467-0ed5f89f718b"
#define DEBUG_RESET_CHARACTERISTIC_UUID "4e9dea23-3da7-11e8-b467-0ed5f89f718b"

// define pins
const int TRIG_PIN = 18;
const int ECHO_PIN = 19;
const int PRIMARY_SOLENOID = 16;
const int LED_STATUS = 17;
const int LED_ERROR = 5;
const int SHUNT_PIN = 36;

// set up non volatile storage
Preferences dataStorage;
unsigned int data_boots;   // total number of data_boots
unsigned int data_fills;   // total number of fills
unsigned int fill_counter; // fill counter for changeout prediction
String data_nickname;

// user set variables
unsigned int FILL_LIMIT;

// set up data for each Booting
int fills = 0;
int condition = 1;  // set initial condition to filling to top off on boot
bool initialBoot = true;

// define the solution level parameters
const int FILL_LEVEL = 1475;    // trigger a fill
const int FULL_LEVEL = 1300;    // stop a fill
const int HIGH_LEVEL = 1250;    // "overflow" point
const int BOTTOM_LEVEL = 1700; // longest possible reading
const int TOP_LEVEL = 1175;    // shortest possible reading
const int DISTANCE_DRIFT = 50;

// variables for sonar readings that are out of range
int errorReadings = 0;    // counter for out of range readings
const int MAX_ERRORS = 5;   // max consecutive error readings

// variables for readings
int currentLevel; // adjusted value that is truncated on bad readings
int rawLevel;     // raw level reading;
int averageLevel;   // average level reading
const int ARRAY_SIZE = 10;    // number of readings to average
int levelArray[ARRAY_SIZE];   // define array for average reading
int arrayIndex = 0;       

// leak detection varialbles
int leakLevel;                         // the starting point level
const int LEAK_DISTANCE = 80;          // the change in distance before alarming a leak
const long LEAK_INTERVAL = 30000;      // time to wait after fill to get reading (30 seconds)
unsigned long LEAK_previousMillis = 0; // last time leak detection was
bool setLeakLevel = false;

unsigned long currentMillis; // current time

// define variables for LED blinking
unsigned long LED_previousMillis = 0; // will store last time LED was updated
const long LED_INTERVAL = 250;        // interval at which to blink (milliseconds)

// define variables for loop delay
unsigned long LEVEL_previousMillis = 0;
const long LEVEL_INTERVAL = 200;    // run loop every 0.2 seconds

// define wave definitions
const int MAX_WAVES = 5;      // the number of consecutive wave readings before error
const int WAVE_DISTANCE = 50; // the distance that is considered a wave
int waveCounter = 0;          // wave counter for erroring
int totalWaveCounter = 0;     // total wave counter for each startup

// define variables for checking solenoid operations
const int SHUNT_LIMIT = 50;
int shuntBaseline;         // shunt reading when solenoid is OFF
const long SHUNT_INTERVAL = 1000;  // delay before checking shunt reading
unsigned long SHUNT_previousMilllis = 0;
bool setShuntBaseline = false;
bool checkShuntReading = false;

// define variables for determining if water is flowing
unsigned long FLOW_previousMillis = 0;  // starting point for checking flow
const long FLOW_INTERVAL = 30000;  // time delay to checking flow (30 seconds)
const int FLOW_LEVEL_CHANGE = 50;   // level increase to verify flow
int flowLevel;     // solution level to compare against
bool checkFlow = false;

unsigned long lastFill;   // time the last fill cycle completed

// define variables for periodic check if solenoid not connected or no water flowing
unsigned long PERIODIC_previousMillis = 0;  // starting point to check periodically during certain errorReadings
const long PERIODIC_INTERVAL = 600000;  // time delay to check again (10 minutes)
bool periodicCheck = false;     // boolean to know when to check periodically


// list all of the functions
void setup();
void loop();
void setup_data();
void setup_ble();
void setup_initialreading();
void level_reading();
void set_condition();
void operate_solenoids();
void update_ble();
void led_control();
bool isWaterFlowing();
bool isSolenoidConnected();
